package hr.fer.projekt.architecture;

import java.util.ArrayList;
import java.util.List;

import hr.fer.projekt.layers.NeuronLayer;
import hr.fer.projekt.learnerFactory.LearnerFactory;
import hr.fer.projekt.learners.Learner;
/**
 * Neuron network architecture abstract class.
 * Contains iterable collection of layers which make up the architecture.
 * Override toString for dumping correct architecture info to file.
 * @author Marin Ov�ari�ek
 *
 */
public abstract class Architecture {
	
	protected List<NeuronLayer> layers;
	protected Learner learner;
	
	protected int inputSize;
	protected int outputSize;
	
	public abstract List<NeuronLayer> getLayers();
	
	public Architecture(LearnerFactory factory) {
		layers=new ArrayList<>();
		this.learner=factory.create();
	}
	
	public void setLayers(List<NeuronLayer> layers) {
		this.layers=layers;
	}
	
	public int getInputSize() {
		return inputSize;
	}
	
	public int getOutputSize() {
		return outputSize;
	}
	public Learner getLearner() {
		return learner;
	}
}
