package hr.fer.projekt.architecture;

import java.util.ArrayList;
import java.util.List;

import hr.fer.projekt.functions.SigmoidDerivative;
import hr.fer.projekt.functions.SigmoidFunction;
import hr.fer.projekt.functions.SoftMax;
import hr.fer.projekt.layers.ConvolutionalLayer;
import hr.fer.projekt.layers.FullyConnectedLayer;
import hr.fer.projekt.layers.HiddenLayer;
import hr.fer.projekt.layers.NeuronLayer;
import hr.fer.projekt.layers.OutputLayer;
import hr.fer.projekt.layers.PoolingLayer;
import hr.fer.projekt.learnerFactory.LearnerFactory;

public class ExtendedMNISTArchitecture extends Architecture {
	private int NO_KERNELS=10;
	private int KERNEL_STRIDE_SIZE=1;
	private int KERNEL_SIZE=5;
	private int POOL_SIZE=2;
	private int POOL_STRIDE_SIZE=2;
	
	private int NO_NEURONS_FULLYCONNECTED=1960;//velicina pool info * broj kernela
	private int NO_HIDDEN1_NEURONS=256;
	private int NO_NEURONS_OUTPUT=10;
	private LearnerFactory factory;

	public ExtendedMNISTArchitecture(LearnerFactory factory) {
		super(factory);
		layers=new ArrayList<>();
		layers.add(new ConvolutionalLayer(KERNEL_STRIDE_SIZE,NO_KERNELS,KERNEL_SIZE,factory.create(), new SigmoidFunction()));
		layers.add(new PoolingLayer(POOL_STRIDE_SIZE,POOL_SIZE));
		layers.add(new FullyConnectedLayer(NO_NEURONS_FULLYCONNECTED));
		layers.add(new HiddenLayer(NO_NEURONS_FULLYCONNECTED, NO_HIDDEN1_NEURONS, factory.create(), new SigmoidFunction(), new SigmoidDerivative()));
		layers.add(new OutputLayer(NO_HIDDEN1_NEURONS,NO_NEURONS_OUTPUT,factory.create(), new SoftMax(),new SigmoidDerivative()));//mozda bi se trebao sigder jer se u konv koristio sig
		setLayers(layers);
		inputSize=layers.get(0).getSize();
		outputSize=layers.get(layers.size()-1).getSize();
	}

	@Override
	public List<NeuronLayer> getLayers() {
		return layers;
	}
	@Override
	public String toString() {
		return "EXTENDED MNIST ARCH";
	}
}
