package hr.fer.projekt.models;

/**
 * Class which represents a matrix with some basic matrix operation.
 * Does not support inverse and transpose.
 * @author Marin Ov�ari�ek
 *
 */
public class Matrix {
	
	private int cols,rows;
	private double[] values;
	/**
	 * 
	 * @param cols
	 * @param rows
	 */
	public Matrix(int cols,int rows) {
		this.cols=cols;
		this.rows=rows;
		values=new double[cols*rows];
	}
	/**
	 * 
	 * @param cols
	 * @param rows
	 * @param values
	 */
	public Matrix(int cols,int rows,double[] values) {
		this(cols,rows);
		if(cols<1 || rows<1 ) {
			throw new IllegalArgumentException();
		}
		if(values.length!=cols*rows) {
			throw new IllegalArgumentException();
		}
		for(int i=0;i<values.length;i++) {
			this.values[i]=values[i];
		}
	}
	
	public Matrix(Matrix mat) {
		this(mat.getCols(),mat.getRows(),mat.getValues());
	}
	//radi
	/**
	 * Basic matrix multiplication O(n^3) complexity.
	 * @param other
	 * @return
	 */
	public Matrix multiply(Matrix other) {
		if(this.cols!=other.getRows()) {
			System.out.println("Ova matrica cols,rows"+this.getCols()+" "+this.getRows());
			System.out.println("Druga matrica cols,rows"+other.getCols()+" "+other.getRows());
			throw new IllegalArgumentException();
		}
		double[] newValues=new double[this.rows*other.getCols()];
		
		 for(int i = 0;i < this.rows;i++){
		      for(int j = 0;j < other.cols;j++){
		    	 newValues[i*other.cols+j]=0;
		         for(int k = 0;k < other.rows;k++){
		            newValues[i*other.cols+j]+=values[i*this.cols+k]*other.getValues()[k*other.cols+j];
		            //values ==this.values
		         }
		      }
		   }
		return new Matrix(other.getCols(),rows,newValues);
	}
	//Gives random values to matrix which will represent weight based on previous and next layer size
	
	
	public int getCols() {
		return cols;
	}


	public int getRows() {
		return rows;
	}

	public double[] getValues() {
		return values;
	}
	
	
	public void setValue(int index,double value) {
		values[index]=value;
	}
	
	/**
	 * Prints the matrix values to standard output.
	 */
	public void print() {
		for(int i=0;i<rows;i++) {
			System.out.println();
			for(int j=0;j<cols;j++) {
				System.out.printf("  %f",values[i*cols+j]);
			}
		}
	}
	
	//radi
	public double dotProduct(Matrix other) {
		if(!(rows==other.getRows() && cols==other.getCols())){
			throw new IllegalArgumentException();
		}
		double result=0;
		for(int i=0;i<values.length;i++) {
			result+=values[i]*other.getValues()[i];
		}
		return result;
	}
	//jako slicno ko maxpool (mogucnost spajanja,lambda izrazi itd.)?
	/**
	 * Performs addition of two matrices.
	 * @param other
	 */
	public void addTo(Matrix other) {
		if(rows!=other.getRows() || cols!=other.getCols()) {
			throw new IllegalArgumentException();
		}
		for(int i=0;i<values.length;i++) {
			values[i]+=other.getValues()[i];
		}
	}
	/**
	 * Performs addition of two matrices.Returns the resulting matrix.
	 * @param other
	 * @return
	 */
	public Matrix addition(Matrix other) {
		Matrix temp=new Matrix(this);
		if(rows!=other.getRows() || cols!=other.getCols()) {
			System.out.println(other.getRows()+" "+other.getCols());
			throw new IllegalArgumentException();
		}
		temp.addTo(other);
		return temp;
	}
	/**
	 * Puts a new value at the specific place in the matrix.
	 * @param index
	 * @param value
	 */
	public void modify(int index,double value) {
		values[index]=value;
	}

	
	public Matrix setValues(double[] values) {
		if(values.length != this.cols*this.rows) {
			throw new IllegalArgumentException();		
			}
		this.values=values;
		return this;
	}
	
	public boolean isAVector() {
		if(cols==1 || rows==1) {
			return true;
		}
		return false;
	}

}
