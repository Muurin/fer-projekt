package hr.fer.projekt.models;

import hr.fer.projekt.artefacts.Artefact;
import hr.fer.projekt.functions.FunctionDerivative;
import hr.fer.projekt.testPackage.IspisProvjera;
/**
 * 
 * @author Marin Ov�ari�ek
 *
 */
public class ErrorCalculator {
	protected double[] output;
	protected double[] expected;
	protected FunctionDerivative derivative;
	/**
	 * Takes a derivate of a custom lossfunction to be used in calculating the gradient.Retruns negative gradient
	 * @param derivative
	 */
	public ErrorCalculator(FunctionDerivative derivative) {
		this.derivative=derivative;
	}
	
	public void set(Artefact a,double[] expected) {
		output=a.getMatrices()[0].getValues();
		this.expected=expected;
	}
	public void setExpected(double[] expected) {
		this.expected = expected;
	}
	public void setOutput(double[] output) {
		this.output = output;
	}
	public Artefact computeError() {
		if(output.length!=expected.length) {
			System.out.println(output.length+" "+expected.length);
			throw new IllegalArgumentException("output size is not same as expected output size!");
		}
		double[] error=new double[output.length];
		double[] differenceOutputExpected=new double[output.length];
		for(int i=0;i<error.length;i++) {
			differenceOutputExpected[i]=(expected[i]-output[i]);
			
			//stavljeno umjesto out(i)-exp(i)
		}
		error=derivative.apply(differenceOutputExpected);
	//	IspisProvjera.ispis(error);
		Matrix[] errorMat= {new Matrix(1,output.length,error)};
		return new Artefact(errorMat);
	}
}
