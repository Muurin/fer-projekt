package hr.fer.projekt.models;

public class PoolInfo {
	private Matrix pooledFeatureMap;
	private int[] pooledIndex;
	
	public PoolInfo(Matrix pooledMatrix,int[] index) {
		this.pooledFeatureMap=pooledMatrix;
		this.pooledIndex=index;
	}
	public Matrix getPooledFeatureMap() {
		return pooledFeatureMap;
	}
	public int[] getPooledIndex() {
		return pooledIndex;
	}
}
