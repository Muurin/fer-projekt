package hr.fer.projekt.models;

import java.util.Random;
import java.util.function.Function;

import hr.fer.projekt.functions.ArrayFunction;

public class MatrixUtility {
	/**
	 * Assings random values to matrix's elements.
	 * @return
	 */
	public static Matrix randomize(Matrix m) {
		Random r =new Random();
		int size=m.getValues().length;
		double[] d=m.getValues();
		for(int i=0;i<size;i++) {
			d[i]=(2*r.nextDouble()-1)*0.01;//*(double)Math.sqrt(2/(input+output));
		}
		return m;
	}
	
	public static Matrix randomizeHe(Matrix m,int noPrevNeurons) {
		Random r =new Random();
		int size=m.getValues().length;
		double[] d=m.getValues();
		for(int i=0;i<size;i++) {
			d[i]=(2*r.nextDouble()-1)*Math.sqrt(2/noPrevNeurons);//*(double)Math.sqrt(2/(input+output));
		}
		return m;
	}
	
	/**
	 * Applies zero value padding around the matrix.
	 * @param paddingSize
	 * @return
	 */
	public static Matrix padding(Matrix m,int paddingSize) {
		if(m.getRows()!=m.getCols()) {
			throw new RuntimeException();
		}
		int newRows=m.getRows()+2*paddingSize;
		int newCols=m.getCols()+2*paddingSize;
		double newValues[]=new double[newRows*newCols];
		for(int i=0; i < m.getRows(); i++) {
			for(int j=0; j < m.getCols(); j++) {
				newValues[(i+paddingSize)*newCols+(j+paddingSize)]=m.getValues()[i*m.getCols()+j];
			}
		}
		return new Matrix(newCols,newRows,newValues);
	}
	
	/**
	 * Constructs a new matrix from the original one which has some elements set to zero with given probability.
	 * @param dropProbability
	 * @return
	 */
	public static Matrix dropout(Matrix m,double dropProbability) {
		Matrix dropped=new Matrix(m);
		int size=m.getValues().length;
		for(int i=0;i<size;i++) {
			if(Math.random()<dropProbability) {
				dropped.setValue(i,0);
			}
		}
		return dropped;
	}
	/**
	 * Extracts values from a matrix with given starting elements and target matrix size.
	 */
	public static double[] valueExtraction(Matrix m, int rowIndex,int colIndex,int receptiveFieldSize) {
		int cols=m.getCols();
		var values=m.getValues();
		double[] extract=new double[receptiveFieldSize*receptiveFieldSize];
		for(int i=0;i<receptiveFieldSize;i++) {
			for(int j=0;j<receptiveFieldSize;j++) {
				if((i+rowIndex)*cols +(j+colIndex)>values.length-1) {
					throw new ArrayIndexOutOfBoundsException();
				}
				extract[i*receptiveFieldSize+j]=values[(i+rowIndex)*cols +(j+colIndex)];
			}
		}
		return extract;
	}
	
	/**
	 * Performs maxpool operation with certain stride over matrix.
	 * @param receptiveFieldSize
	 * @param stride
	 * @return
	 */
	public static PoolInfo maxPooling(Matrix m,int receptiveFieldSize,int stride) {
		int pooledSize=(m.getRows()-receptiveFieldSize)/stride + 1,cnt=0; 
		double pooled[]=new double[pooledSize*pooledSize];
		
		int pooledIndex[]=new int[pooledSize*pooledSize];
		
		int limit=m.getRows()-receptiveFieldSize+1;
		
		for(int i=0;i<limit;i+=stride) {
			for(int j=0;j<limit;j+=stride) {
				pooled[cnt]=returnMax(MatrixUtility.valueExtraction(m,i,j, receptiveFieldSize));
				int maxIndex=returnMaxIndex(MatrixUtility.valueExtraction(m,i,j, receptiveFieldSize));
				pooledIndex[cnt++]=(i+maxIndex/receptiveFieldSize)/* */*(m.getCols())
						+j //pocetna pozicija
						+maxIndex%receptiveFieldSize;
				
			}
		}
		return new PoolInfo(new Matrix(pooledSize,pooledSize,pooled),pooledIndex);
		
	}
	/**
	 * Returns index of the biggest element in the array.
	 * @param array
	 * @return
	 */
	private static int returnMaxIndex(double[] array) {
		double max=array[0];
		int index=0;
		for(int i=0;i<array.length;i++) {
			if(max<array[i]) {
				max=array[i];
				index=i;
			}
		}
		return index;
	}
	/**
	 * Returns the biggest element in the array.
	 * @param array
	 * @return
	 */
	private static double returnMax(double[] array) {
		double max=array[0];
		for(double f:array) {
			if(max<f) {
				max=f;
			}
		}
		return max;
	}
	/**
	 * Extracts a smaller matrix with defined starting element and size.
	 * @param rowIndex
	 * @param colIndex
	 * @param receptiveFieldSize
	 * @return
	 */
	public static Matrix matrixExtraction(Matrix m,int rowIndex,int colIndex,int receptiveFieldSize) {
		return new Matrix(receptiveFieldSize,receptiveFieldSize,MatrixUtility.valueExtraction(m,rowIndex,colIndex,receptiveFieldSize));
	}
	
	public static Matrix pooledStretchedDelta(PoolInfo info,int size,double[] delta) {
		Matrix stretched=new Matrix(size, size);
		int[] index=info.getPooledIndex();
		for(int i=0;i<delta.length;i++) {
			stretched.modify(index[i],delta[i]);
		}
		return stretched;
	}
	
	/**
	 * Performs convolution operation between two matrices.
	 * @param kernel
	 * @param stride
	 * @return
	 */
	public static Matrix convolve(Matrix m,Matrix kernel,int stride) {
		int recpFieldSize=kernel.getCols();
		int rows=m.getRows();
		int cols=m.getCols();
		int featureSize=(rows- recpFieldSize)/stride + 1,cnt=0; 
		double feature[]=new double[featureSize*featureSize];
		for(int i=0;i<rows-recpFieldSize+1;i+=stride) {
			for(int j=0;j<cols-recpFieldSize+1;j+=stride) {
				feature[cnt++]=MatrixUtility.matrixExtraction(m,i,j, recpFieldSize).dotProduct(kernel);
			}
		}
		return new Matrix(featureSize,featureSize,feature);
	}
	
	/**
	 * Applies a function on every element in the matrix.
	 * @param function
	 * @return
	 */
	public static Matrix transform(Matrix m,ArrayFunction function) {
		m.setValues(function.apply(m.getValues()));
		return m;
	}

//	public static Matrix transform(Matrix m,Function<Double, Double> function) {
//		double[] v=m.getValues();
//		int size=v.length;
//		for(int i=0;i<size;i++) {
//			v[i]=function.apply(v[i]);
//		}
//		return  m;
//		
//	}

}
