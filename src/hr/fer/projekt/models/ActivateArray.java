package hr.fer.projekt.models;

public interface ActivateArray {
	public double[] activate(double[] d);

}
