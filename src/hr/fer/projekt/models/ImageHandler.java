package hr.fer.projekt.models;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
/**
 * Class that converts an image file to appropriate format for testing.
 * 
 */
public class ImageHandler {
	private BufferedImage image;
	private double[] pixels;
	private byte[] bytePixels;
	
	private int imageWidth=28;
	private int imageHeight=28;
	private int scanSize=28;


	public ImageHandler(File file) {
		try {
		    image =ImageIO.read(file);
		} catch (IOException e) { 
		    e.printStackTrace(); 
		}
		//System.out.println("width"+image.getWidth()+"height"+image.getHeight());
		//crops a square area from the picture,and resizes it to 28x28
		//image=image.getSubimage(10, 2519, 1512,1512);
		//image = resize(image, 28, 28);
		
		image=convertToGrayscale(image);
		pixels=intTodoubleArray(image.getRGB(0, 0, imageWidth, imageHeight, null, 0, scanSize));
		bytePixels=intToByteArray(image.getRGB(0, 0, imageWidth,imageHeight, null, 0, scanSize));
		
	}
	
	public BufferedImage convertToGrayscale(BufferedImage image) {
		int height=image.getHeight();
		int width=image.getWidth();
		for(int y = 0; y < height; y++){
			for(int x = 0; x < width; x++){
		        int p = image.getRGB(x,y);//u p su spremljenje ARGB vrijednosti (svaka po 8 bita)
		        //izvlacenje pojedinacnih vrijednosti
		        int a = (p>>24)&0xff;
		        int r = (p>>16)&0xff;
		        int g = (p>>8)&0xff;
		        int b = p&0xff;

		        //calculate average
		        int avg = (r+g+b)/3;

		        //replace RGB value with avg
		        p = (a<<24) | (avg<<16) | (avg<<8) | avg;
		        if(avg>140)p=0;//filtar za prljavu bijelu
		        image.setRGB(x, y, p);
		      }
		    }
		return image;
	}
	//also reverses the grayscale value
	public double[] intTodoubleArray(int[] intArray) {
		int length=intArray.length;
		double[] doubleArray=new double[length];
			for(int i=0;i<length;i++) {
				intArray[i]=256-intArray[i];
				doubleArray[i]=(double) intArray[i]/256;
			}
		return doubleArray;
	}
	
	public byte[] intToByteArray(int[] intArray) {
		int length=intArray.length;
		byte[] byteArray=new byte[length];
			for(int i=0;i<length;i++) {
				byteArray[i]=(byte)intArray[i];
			}
		return byteArray;
	}

	public double[] getPixels() {
		return pixels;
	}
	
	public byte[] getBytepixels() {
		return bytePixels;
	}
	
	public byte[] reversePixels() {
		int length=bytePixels.length;
		int temp;
		byte[] reversed=new byte[length];
		for(int i=0;i<length;i++) {
			temp=256-(int) bytePixels[i];
			reversed[i]=(byte)temp;
		}
		return reversed;
	}
	private static BufferedImage resize(BufferedImage img, int height, int width) {
        Image tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = resized.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();
        return resized;
    }
}
