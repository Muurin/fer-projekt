package hr.fer.projekt.functions;

public interface FunctionDerivative{
	public double[] apply(double[] output);
}
