package hr.fer.projekt.functions;

import java.util.Arrays;

public class IdentityFunction implements ArrayFunction{

	@Override
	public double[] apply(double[] values) {
		return Arrays.copyOf(values,values.length);
	}
	
}
