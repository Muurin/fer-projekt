package hr.fer.projekt.functions;

import hr.fer.projekt.networks.NeuralNetwork;
/**
 * 
 * @author Marin Ov�ari�ek
 *
 */
public class SoftMax implements ArrayFunction {

	@Override
	public double[] apply(double[] values) {
		double sum=0;
		double[] soft=new double[values.length];
		double max = values[NeuralNetwork.max(values)];
		for(double d:values) {
			sum+=Math.exp(d-max);
		}
		for(int i=0;i<values.length;i++) {
			soft[i]=Math.exp(values[i]-max)/sum;
		}
		return soft;
	}

}
