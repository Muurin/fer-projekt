package hr.fer.projekt.functions;

public interface ArrayFunction{
	public double[] apply(double[] values);
}
