package hr.fer.projekt.functions;
/**
 * 
 * @author Marin Ov�ari�ek
 *
 */
public class SigmoidDerivative implements FunctionDerivative{

	@Override
	public double[] apply(double[] output) {
		double[] result=new double[output.length];
		double[] sig=new SigmoidFunction().apply(output);
		for(int i=0;i<output.length;i++) {
			result[i]=(1-sig[i])*sig[i];
		}
		return result;
	}

}
