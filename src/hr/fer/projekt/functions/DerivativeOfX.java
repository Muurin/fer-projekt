package hr.fer.projekt.functions;

public class DerivativeOfX implements FunctionDerivative {
	//stavlja se kao derivative aktivacijske funkcije od fullyconnected (aktivacijska fja je  f(y)=x)=> der(f(y)=1)
	@Override
	public double[] apply(double[] output) {
		double[] der=new double[output.length];
		for(int i=0;i<der.length;i++) {
			der[i]=1;
		}
		return der;
	}

}
