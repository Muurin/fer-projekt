package hr.fer.projekt.functions;

/**
 * 
 * @author Marin Ov�ari�ek
 *
 */
public class SigmoidFunction implements ArrayFunction{

	@Override
	public double[] apply(double[] t) {
		double[] temp=new double[t.length];
		for(int i=0;i<t.length;i++) {
			temp[i]=1/(1+Math.exp(-t[i]));
		}
		return temp;
	}
}
