package hr.fer.projekt.networks;
/**
 * Class containing network parameters.
 * @author Marin Ov�ari�ek
 *
 */
public class NetworkSettings {
	private int PaddingSize;
	private int batchSize;
	private int noEpochs;
	private int noKernels;
	private int kernelSize;
	
	public NetworkSettings(int paddingSize, int batchSize, int noEpochs, int noKernels, int kernelSize) {
		PaddingSize = paddingSize;
		this.batchSize = batchSize;
		this.noEpochs = noEpochs;
		this.noKernels = noKernels;
		this.kernelSize = kernelSize;
	}

	public int getPaddingSize() {
		return PaddingSize;
	}

	public int getBatchSize() {
		return batchSize;
	}

	public int getNoEpochs() {
		return noEpochs;
	}

	public int getNoKernels() {
		return noKernels;
	}

	public int getKernelSize() {
		return kernelSize;
	}
	
	
	@Override
		public String toString() {
			return "Batch size: "+batchSize+System.lineSeparator()
					+"Number of epochs: "+noEpochs+System.lineSeparator()
					+"Number of kernels: "+noKernels+System.lineSeparator()
					+"Kernel size: "+kernelSize+System.lineSeparator();
		}
	
	
	
}
