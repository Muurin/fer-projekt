package hr.fer.projekt.networks;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JPanel;

import hr.fer.projekt.architecture.Architecture;
import hr.fer.projekt.artefacts.Artefact;
import hr.fer.projekt.dataloader.Dataset;
import hr.fer.projekt.models.ErrorCalculator;
import hr.fer.projekt.models.ImageHandler;
import hr.fer.projekt.models.Matrix;
import hr.fer.projekt.models.MatrixUtility;
import hr.fer.projekt.statistics.MyNetworkStatistics;
import hr.fer.projekt.statistics.MyStatisticsAgregator;
import hr.fer.projekt.statistics.NetworkStatistics;
import hr.fer.projekt.dataloader.ImageOutputPair;
import hr.fer.projekt.functions.FunctionDerivative;
import hr.fer.projekt.layers.NeuronLayer;
/**
 * Neuron network implementation.
 * @author Marin Ov�ari�ek
 *
 */
public class NeuralNetwork implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private enum Testing{
		ContinousTest,LastTest;
	}
	
	Architecture architecture;
	NetworkSettings settings;
	Dataset trainSet;
	Dataset testSet;
	NetworkStatistics statistics;
	MyStatisticsAgregator agregator;
	
	FunctionDerivative lossDerivative;
	
	int sampleFrequency;
	
	public NeuralNetwork(Architecture architecture,Dataset trainSet,Dataset testSet, NetworkSettings settings,MyStatisticsAgregator agregator,FunctionDerivative lossDerivative) {
		this.architecture=architecture;
		this.trainSet=trainSet;
		this.testSet=testSet;
		this.settings=settings;
		this.agregator=agregator;
		
		this.lossDerivative=lossDerivative;
		
		setUpStatistics();
	}
	
	public void saveState(File file) {
		try {
			ObjectOutputStream os=new ObjectOutputStream(new FileOutputStream(file));
			os.writeObject(this);
			os.close();
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	public static  NeuralNetwork loadState(File file) {
		try {
			ObjectInputStream is=new ObjectInputStream(new FileInputStream(file));
			NeuralNetwork network=(NeuralNetwork) is.readObject();
			is.close();
			return network;
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}catch(IOException e) {
			e.printStackTrace();
		}catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.err.println("LOAD=NULL");
		return null;
	}
	//Implement statistic collection
	public void train() {
		
		List<NeuronLayer> layers=architecture.getLayers();
		List<ImageOutputPair> samples=trainSet.getData();
		int size=samples.size();//size ili TEST_SIZE
		int noLayers=architecture.getLayers().size();
		ErrorCalculator outputError=new ErrorCalculator(lossDerivative);
		int noEpochs=settings.getNoEpochs();
		int noGraphSamples=agregator.getNumberOfGraphSamples();
		int batchSize=settings.getBatchSize();
		
		//PROBNO
		int temporarySize=5000;
		//size=temporarySize;
		
		int statCountDown=0; 
		
		for(int i=0;i<noEpochs;i++) {
			Collections.shuffle(samples);
			//size ili TEST_SIZE
			for(int j=0;j<size;j++) {
				//uzmi sliku 
				Artefact a=new Artefact(new Matrix[]{prepareImage(samples.get(j).getImageValues())});
				a=feedForward(a);
				
				//izracunaj gresku
				outputError.setOutput(a.getMatrices()[0].getValues());
				outputError.setExpected(expectedOutput(samples.get(j).getOutputValue()));
				a=outputError.computeError();
				
				//backPropagate
				backpropagate(a);
				
				//update
				if((j+1) % batchSize==0) {
				//	System.out.println("Update");
					for(int l=0;l<noLayers;l++) {
						layers.get(l).update();
					}	
				//	System.out.println("Odradio "+batchSize);
				}
				int tempSampleFreq=2500;
				//sampleFrequency=tempSampleFreq;
				if((statCountDown+1)%sampleFrequency ==0 && sampleFrequency!=0) {
					statCountDown=0;
					//System.out.println("Pozvan cont test");
			//	System.out.println("Frekvencija je "+sampleFrequency+"Trenutna slika je "+j+1);
				test(Testing.ContinousTest);
					
				}
				statCountDown++;  
			}
		}
		test(Testing.LastTest);
		
	}
	
	public void test(Testing testing) {
		boolean correct=false;
		int correctGueses=0;
		List<ImageOutputPair> samples=testSet.getData();
		int size=samples.size();//size ili TEST_SIZE
		for(int i=0;i<size;i++) {
			correct=false;
			Artefact a=new Artefact(new Matrix[]{prepareImage(samples.get(i).getImageValues())});
			a=feedForward(a);
			int label=samples.get(i).getOutputValue();// redni broj klase
			
			if(max(a.getMatrices()[0].getValues()) == label) {
					correctGueses++;
					correct=true;
			}
			if(testing==Testing.ContinousTest) {
				statistics.addNewSample(label, correct);
			}
		}
		//System.out.println(correctGueses/size);
	if(testing==Testing.LastTest) {
		statistics.setTotalAccuracy((double)correctGueses/size);
		//System.out.println((double)correctGueses/size);
	}
	else if(testing==Testing.ContinousTest) {
		statistics.calculateAverages();
	}
		
	}
	//===================
	
	protected Artefact feedForward(Artefact a){
		List<NeuronLayer> layers=architecture.getLayers();
		int noLayers=layers.size();
		for(int l=0;l<noLayers;l++) {
			NeuronLayer current=architecture.getLayers().get(l);
			current.setForward(a);
			a=current.feedForward();
		}
		return a;
	}
	
	private void backpropagate(Artefact a) {
		List<NeuronLayer> layers=architecture.getLayers();
		int noLayers=layers.size();
		
		for(int l=noLayers-1;l>=0;l--){
			NeuronLayer current=layers.get(l);
			current.setBackward(a);
			a=current.backPropagate();
		}
	}
	
	public  void testSample() {
		JPanel panel=new JPanel();
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
		int result = fileChooser.showOpenDialog(panel);
		if (result == JFileChooser.APPROVE_OPTION) {
		    File selectedFile = fileChooser.getSelectedFile();
		    ImageHandler handler=new ImageHandler(selectedFile);
		    Matrix[] input= {prepareImage(handler.getPixels())};
		    Artefact a=new Artefact(input);
			a=feedForward(a);
			
			System.out.println("This image represents digit "+max(a.getMatrices()[0].getValues()));
		}
	}
	
	protected Matrix prepareImage(double[] values) {
		int imageWidth=(int)Math.sqrt(trainSet.getData().get(0).getImageValues().length);
		int imageHeight=imageWidth;
		return MatrixUtility.padding(new Matrix(imageWidth,imageHeight,values),settings.getPaddingSize());
	}
	
	protected double[] expectedOutput(int output) {
		int size=trainSet.getClasses().size();
		double[] outputArray=new double[size];
		outputArray[output]=1;
		return outputArray;
	}
	
	public static int max(double[] d) {
		double max=d[0];//Double.MIN_VALUE;
		int maxIndex=0;//-1;
		for(int i=0;i<d.length;i++) {
			if(d[i]>max) {
				max=d[i];
				maxIndex=i;
			}
		}
		return maxIndex;
	}
	
	@Override
	public String toString() {
		return "\n"+architecture.toString()+System.lineSeparator()
				+settings.toString()+System.lineSeparator();
	}
	
	public void setUpStatistics() {
		statistics=new MyNetworkStatistics();
		statistics.setNetworkInfo(this.toString());
		statistics.setNumberOfClasses(trainSet.getClasses().size());
		statistics.setNumberOfGraphSamples(agregator.getNumberOfGraphSamples());
		statistics.setNumberOfTrainSamples(testSet.getData().size()*settings.getNoEpochs());
		agregator.addStatistic(statistics);
		agregator.setInfo(architecture.getLearner().toString());
		
		sampleFrequency=trainSet.getData().size()*settings.getNoEpochs()//kolicina podataka
							/agregator.getNumberOfGraphSamples();
	//	System.out.println(sampleFrequency);
	}
	
	
}

