package hr.fer.projekt.layers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import hr.fer.projekt.artefacts.Artefact;
import hr.fer.projekt.functions.ArrayFunction;
import hr.fer.projekt.learners.Learner;
import hr.fer.projekt.models.Matrix;
import hr.fer.projekt.models.MatrixUtility;
import hr.fer.projekt.testPackage.IspisProvjera;

public class ConvolutionalLayer extends NeuronLayer{
	
	protected Matrix values;
	protected int stride;
	protected List<Matrix> kernels;
	protected List<Matrix> deltaKernels;
	protected Learner learner;
	protected ArrayFunction activationFunction;
	
	public ConvolutionalLayer(int stride, int noKernels,int kernelSize,Learner learner,ArrayFunction activationFunction) {
		super();
		this.stride = stride;
		this.activationFunction=activationFunction;
		kernels=new ArrayList<Matrix>();
		deltaKernels=new ArrayList<Matrix>();
		this.learner=learner;
		for(int i=0;i<noKernels;i++) {
			kernels.add(MatrixUtility.randomize(new Matrix(kernelSize, kernelSize)));
			deltaKernels.add(new Matrix(kernelSize, kernelSize));
		}
	}

	@Override
	public Artefact feedForward() {
		List<Matrix> featureMaps=new ArrayList<Matrix>();
		//IspisProvjera.ispis(values.getValues());
		for(Matrix kernel:kernels) {
			featureMaps.add(MatrixUtility.transform(MatrixUtility.convolve(values, kernel, stride),activationFunction));
		}
		int size=featureMaps.size();
		Matrix[] result=new Matrix[size];
		featureMaps.toArray(result);
//		for(int i=0;i<size;i++) {
//			result[i]=featureMaps.get(i);
//		}
		//IspisProvjera.ispis(result[3].getValues());
		return new Artefact(result);
	}
	
	@Override
	public void setForward(Artefact a) {
		values=a.getMatrices()[0];
	}
	@Override
	public  void setBackward(Artefact delta) {
		List<Matrix> gradients=new ArrayList<>(Arrays.asList(delta.getMatrices()));
		//IspisProvjera.ispis(gradients.get(0).getValues());
		//modifikacija gradijenta learnerom
		for(int i=0;i<deltaKernels.size();i++) {
		//	deltaKernels.get(i).setValues(learner.calculateDeltaWeights(deltaKernels.get(i).getValues()));//primjeni learner na gradijente
			deltaKernels.set(i,deltaKernels.get(i).addition(MatrixUtility.convolve(values,gradients.get(i), stride)));//obaci konvoluciju i rezultat nadodaj u deltaKernel spremnik koji ce pozivom update() promijeniti filtere
		}
		//IspisProvjera.ispis(deltaKernels.get(3).getValues());
		return;
	}
	/*
	 * Kako ce izgledati backpropagate ako se nadoveze par convolutional layera???
	 * Kako bi izgledao delta za konv?
	 */
	@Override
	public  Artefact backPropagate() {
		return null;
	}
	
	@Override
	public void update() {
		int size=kernels.size();
		//IspisProvjera.ispis(kernels.get(4).getValues());
		for(int i=0;i<size;i++) {
			kernels.set(i,kernels.get(i).addition(deltaKernels.get(i)));
		}
		for(Matrix kernel:deltaKernels)
			MatrixUtility.transform(kernel, matrixValues->{
				for(int i=0;i<matrixValues.length;i++) {
					matrixValues[i]=0;
				}
				return matrixValues;});
		
		//IspisProvjera.ispis(kernels.get(4).getValues());
	}

}
