package hr.fer.projekt.layers;

import java.util.ArrayList;
import java.util.List;

import hr.fer.projekt.artefacts.Artefact;
import hr.fer.projekt.functions.ArrayFunction;
import hr.fer.projekt.learners.Learner;
import hr.fer.projekt.models.Matrix;
import hr.fer.projekt.models.MatrixUtility;

public class NoCrunchConvolutionalLayer extends ConvolutionalLayer {

	public NoCrunchConvolutionalLayer(int stride, int noKernels, int kernelSize, Learner learner,
			ArrayFunction activationFunction) {
		super(stride, noKernels, kernelSize, learner, activationFunction);
		// TODO Auto-generated constructor stub
	}
	@Override
	public Artefact feedForward() {
		List<Matrix> featureMaps=new ArrayList<Matrix>();
		//IspisProvjera.ispis(values.getValues());
		for(Matrix kernel:kernels) {
			featureMaps.add((MatrixUtility.convolve(values, kernel, stride)));
		}
		int size=featureMaps.size();
		Matrix[] result=new Matrix[size];
		featureMaps.toArray(result);
//		for(int i=0;i<size;i++) {
//			result[i]=featureMaps.get(i);
//		}
		//IspisProvjera.ispis(result[3].getValues());
		return new Artefact(result);
	}

}
