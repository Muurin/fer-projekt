package hr.fer.projekt.layers;

import hr.fer.projekt.artefacts.Artefact;
import hr.fer.projekt.functions.ArrayFunction;
import hr.fer.projekt.functions.FunctionDerivative;
import hr.fer.projekt.learners.Learner;
import hr.fer.projekt.models.ActivateArray;
import hr.fer.projekt.models.Matrix;
import hr.fer.projekt.models.MatrixUtility;
import hr.fer.projekt.testPackage.IspisProvjera;

public class OutputLayer extends NeuronLayer {
	protected Matrix weights;
	protected Matrix deltaWeights;
	protected Matrix prevNeuronValues;
	protected Matrix neuronValues;

	
	protected Learner learner;
	protected ArrayFunction activation;
	FunctionDerivative derivative;
	
	protected double[] error;
	
	double bias=0.0001;
	
	public OutputLayer(int prevNeurons, int noNeurons,Learner learner,ArrayFunction activation,FunctionDerivative derivative) {
		weights=MatrixUtility.randomize(new Matrix(prevNeurons,noNeurons));
		deltaWeights=new Matrix(prevNeurons,noNeurons);
		this.learner=learner;
		this.activation=activation;
		this.derivative=derivative;
	}
	
	@Override
	public Artefact feedForward() {
		//weights(matrix-noOutputNeurons x noPrevNeurons  ) X prevValues(vector-noPrevnNeurons x 1)
		neuronValues=weights.multiply(prevNeuronValues);
		MatrixUtility.transform(neuronValues, arrayValues->{
			for(int i=0;i<arrayValues.length;i++) {
				arrayValues[i]+=bias;
			}
			return arrayValues;});//+bias
		
		double[] soft=activation.apply(neuronValues.getValues());
		neuronValues.setValues(soft);
		//Matrix[] output= {new Matrix(1, soft.length,soft)};
		Matrix[] output=new Matrix[]{neuronValues};
		//IspisProvjera.ispis(output[0].getValues());
		return new Artefact(output);
		
	
	}
	
	@Override
	public void setForward(Artefact a) {
		prevNeuronValues=a.getMatrices()[0];
	}
	@Override
	public void setBackward(Artefact a) {
		error=a.getMatrices()[0].getValues();
	}
	
	@Override
	public Artefact backPropagate() {
		//deltaWeights
		double[] prevActivations=prevNeuronValues.getValues();
		double[] currentDeltaW=new double[deltaWeights.getValues().length];
		double[] lossGradients=new double[deltaWeights.getValues().length];
		
		int valuesSize=neuronValues.getRows();
		int prevValuesSize=prevNeuronValues.getRows();
		for(int i=0;i<prevValuesSize;i++) {
			for(int j=0;j<valuesSize;j++) {
				lossGradients[j*prevValuesSize+i]+=error[j]*prevActivations[i];
			}
		}
		
		currentDeltaW=learner.calculateDeltaWeights(lossGradients);//trenutni gradijent pomnozem vrijednoscu metode ucenje

		deltaWeights=deltaWeights.addition(new Matrix(prevValuesSize, valuesSize, currentDeltaW));
		//IspisProvjera.ispis(deltaWeights.getValues());
		//vraca error za backprop layeru prije
		double[] propError=new double[prevNeuronValues.getValues().length];
		double[] der=derivative.apply(prevActivations);
		
		for(int i=0;i<propError.length;i++) {//od prijasnjeg layera
			for(int j=0;j<error.length;j++) {//od ovog layera
				propError[i] += weights.getValues()[j*propError.length+i] //parcNetO/parcOutH
						* error[j] ;//parc Eo/parcNetO
			}
			propError[i]*=der[i]; //parcEtotal/parcOutH1 * parcOut/parcNetH1
			
		}
		//IspisProvjera.ispis(propError);
		Matrix[] result= {new Matrix(1,propError.length,propError)};
		return new Artefact(result);
	}
	
	@Override
	public void update() {
		weights.addTo(deltaWeights);
		//nakon batcha stavit delte na nula
		MatrixUtility.transform(deltaWeights, matrixValues->{
			for(int i=0;i<matrixValues.length;i++) {
				matrixValues[i]=0;
			}
			return matrixValues;});
		//IspisProvjera.ispis(weights.getValues());
		return;
	}
	public Matrix getWeights() {
		return weights;
	}
}
