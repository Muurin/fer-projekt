package hr.fer.projekt.layers;

import java.util.ArrayList;
import java.util.List;

import hr.fer.projekt.artefacts.Artefact;
import hr.fer.projekt.models.PoolInfo;
import hr.fer.projekt.testPackage.IspisProvjera;
import hr.fer.projekt.models.Matrix;
import hr.fer.projekt.models.MatrixUtility;

public class PoolingLayer extends NeuronLayer{
	private Matrix[] featureMaps;
	private List<PoolInfo> pooledInfo;//treba resetirati pooled
	private int stride;
	private int poolFieldSize;
	
	private Matrix[] error;//ERROR CE BITI DOUBLE[]
	
	public PoolingLayer(int stride,int poolFieldSize) {
		this.stride=stride;
		this.poolFieldSize=poolFieldSize;
	}
	@Override
	public void setForward(Artefact a) {
		featureMaps= a.getMatrices();
	}
	
	@Override
	public Artefact feedForward() {
		pooledInfo=new ArrayList<PoolInfo>();//reset
		Matrix[] pooled=new Matrix[featureMaps.length];//salje se dalje
		for(int i=0;i<featureMaps.length;i++) {
			pooledInfo.add(MatrixUtility.maxPooling(featureMaps[i], poolFieldSize, stride));//za sebe sprema svu inforamciju poolanja
			pooled[i]=pooledInfo.get(i).getPooledFeatureMap();//dalje proslijeduje samo poolane matrice
		}
	//	IspisProvjera.ispis(pooled[0].getValues());
		return new Artefact(pooled);
	}
	
	@Override
	public  void setBackward(Artefact d) {
		error= d.getMatrices();
	//	IspisProvjera.ispis(error[0].getValues());
	}
	
	@Override
	public Artefact backPropagate() {
		int size=featureMaps[0].getCols();
		Matrix[] gradients=new Matrix[error.length];
		for(int i=0;i<error.length;i++) {
		gradients[i]=(MatrixUtility.pooledStretchedDelta(pooledInfo.get(i), size, error[i].getValues()));// jos treba konvoluirati
		}
		return new Artefact(gradients);
	}
	@Override
	public void update() {
	}
	
}
