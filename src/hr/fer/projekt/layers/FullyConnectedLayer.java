package hr.fer.projekt.layers;

import hr.fer.projekt.artefacts.Artefact;
import hr.fer.projekt.models.Matrix;
/**
 * Layer that performs stretching of mutliple pooling outputs into one layer in forward feednig, and splits gradients into multiple
 * parts in backpropagation.
 * @author Marin Ov�ari�ek
 *
 */
public class FullyConnectedLayer extends NeuronLayer{
	
	private Matrix[] input;
	double[] error;
	
	public FullyConnectedLayer(int size) {
		this.size=size;
	}

	@Override
	public Artefact feedForward() {
		double[] joinedValues=new double[size];
		int cnt=0;
		for(Matrix matrix:input) {
			double[] field=matrix.getValues();
				for(int i=0;i<field.length;i++) {
					joinedValues[cnt]=field[i];
					cnt++;
				}
		}
		Matrix[] outputArray= {new Matrix(1,size,joinedValues)};
		return new Artefact(outputArray);
	}

	@Override
	public void setForward(Artefact a) {
		input=a.getMatrices();
	}

	@Override
	public Artefact backPropagate() {
		int size=input[0].getCols();
		int noOfFilters=input.length;
		int z=0;
		int sizeSquared=size*size;
		Matrix[] delta=new Matrix[noOfFilters];
		for(int i=0;i<noOfFilters;i++) {
			delta[i]=new Matrix(size,size);//nova matrica
			double[] d=new double[sizeSquared];//nove vrijednosti matrice
			for(int j=0;j<size*size;j++) {
				d[j]=error[z++];
			}
			delta[i].setValues(d);//postavi vrijednost matrice
		}
		return new Artefact(delta);
	}

	@Override
	public void setBackward(Artefact d) {
		error=d.getMatrices()[0].getValues();
	}

	@Override
	public void update() {
	}

}
