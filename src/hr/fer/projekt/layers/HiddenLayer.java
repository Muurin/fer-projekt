package hr.fer.projekt.layers;

import hr.fer.projekt.artefacts.Artefact;
import hr.fer.projekt.functions.ArrayFunction;
import hr.fer.projekt.functions.FunctionDerivative;
import hr.fer.projekt.learners.Learner;
import hr.fer.projekt.models.ActivateArray;

import hr.fer.projekt.models.Matrix;
import hr.fer.projekt.models.MatrixUtility;

public class HiddenLayer extends NeuronLayer {
	protected Matrix weights;
	protected Matrix deltaWeights;
	protected Matrix prevNeuronValues;
	protected Matrix neuronValues;
	protected double bias=0.0001;
	
	protected Learner learner;
	protected ArrayFunction activation;
	protected FunctionDerivative derivative;
	
	protected double[] error;
	
	public HiddenLayer(int prevNeurons, int noNeurons,Learner learner,ArrayFunction activation,FunctionDerivative derivative) {
		weights=MatrixUtility.randomize(new Matrix(prevNeurons,noNeurons));
		deltaWeights=new Matrix(prevNeurons,noNeurons);
		this.activation=activation;
		this.derivative=derivative;
		this.learner=learner;
	}
	
	@Override
	public Artefact feedForward() {
		//weights(matrix-noOutputNeurons x noPrevNeurons  ) X prevValues(vector-noPrevnNeurons x 1)
		neuronValues=weights.multiply(prevNeuronValues);
		MatrixUtility.transform(neuronValues, arrayValues->{
			for(int i=0;i<arrayValues.length;i++) {
				arrayValues[i]+=bias;
			}
			return arrayValues;});//+bias
		
		double[] soft=activation.apply(neuronValues.getValues());
		neuronValues.setValues(soft);
		Matrix[] output=new Matrix[]{neuronValues};
		return new Artefact(output);
		
	
	}
	
	@Override
	public void setForward(Artefact  a) {
		prevNeuronValues=a.getMatrices()[0];
	}
	@Override
	public void setBackward(Artefact d) {
		error=d.getMatrices()[0].getValues();
	}
	
	@Override
	public Artefact backPropagate() {
		//deltaWeights
		double[] prevActivations=prevNeuronValues.getValues();
		double[] currentDeltaW=new double[deltaWeights.getValues().length];
		double[] lossGradients=new double[deltaWeights.getValues().length];
		
		int valuesSize=neuronValues.getRows();
		int prevValuesSize=prevNeuronValues.getRows();
		for(int i=0;i<prevValuesSize;i++) {
			for(int j=0;j<valuesSize;j++) {
				lossGradients[j*prevValuesSize+i]+=error[j]*prevActivations[i];
			}
		}
		//izracunava se promijena na tezinama
		currentDeltaW=learner.calculateDeltaWeights(lossGradients);
		
		//treba doddati currentDeltaW=learner.deltaWeights()
		deltaWeights=deltaWeights.addition(new Matrix(prevValuesSize, valuesSize, currentDeltaW));
		
		//vraca error za backprop layeru prije
		/*
		 * Layer mora izracunati gresku svakon neurona i pozbrojit ju kako bi vratio ukupan vektor greske prethodnom neuronu
		 */
		double[] propError=new double[prevNeuronValues.getValues().length];
		double[] der=derivative.apply(prevActivations);
		for(int i=0;i<propError.length;i++) {
			for(int j=0;j<error.length;j++) {
				//error od prethodnog
				propError[i] += weights.getValues()[j*propError.length+i] 
						* error[j] ;
				
			}
			propError[i]*=der[i];
		}
		Matrix[] result= {new Matrix(1,propError.length,propError)};
		return new Artefact(result);
	}
	
	@Override
	public void update() {
		weights.addTo(deltaWeights);
		//nakon batcha stavit delte na nula
		MatrixUtility.transform(deltaWeights, matrixValues->{
			for(int i=0;i<matrixValues.length;i++) {
				matrixValues[i]=0;
			}
			return matrixValues;});
	}
}
