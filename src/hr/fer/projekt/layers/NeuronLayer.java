package hr.fer.projekt.layers;

import hr.fer.projekt.artefacts.Artefact;

public abstract class NeuronLayer {
	protected int size;
	
	public abstract Artefact feedForward();
	
	public abstract void setForward(Artefact a);
	
	public abstract Artefact backPropagate();
	
	public abstract void setBackward(Artefact d);
	
	public abstract void update();
	
	public int getSize() {
		return size;
	}
}
