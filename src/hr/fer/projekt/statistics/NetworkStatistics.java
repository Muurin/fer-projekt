package hr.fer.projekt.statistics;

public interface NetworkStatistics {
	
	public void setNumberOfClasses(int numberOfClasses);
	public void setNumberOfTrainSamples(int numberOfTestSamples);
	public void addNewSample(int ordinalOfClass,boolean correct);
	public void calculateAverages();
	public void setNetworkInfo(String info);
	public void setNumberOfGraphSamples(int n);
	public int getNumberOfClasses();
	public double getTotalAccuracy();
	public double[][] getSpecificAccuracySteps();
	public double[] getTotalAccuracySteps();
	public double[] getSpecificAccuracy();
	public void setTotalAccuracy(double totalAccuracy);
}
