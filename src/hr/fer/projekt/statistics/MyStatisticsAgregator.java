package hr.fer.projekt.statistics;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class MyStatisticsAgregator {
	private String filePath;
	private List<NetworkStatistics> statistics;
	private int numberOfGraphSamples;
	
	
	double totalAverage;//TREBA ISPISATI
	double totalMin;
	double totalMax=0;
	int numberOfClasses;
	double[] specificAverages;
	
	double[] totalAverageSteps;
	double[][] specificAccuracySteps;
	private String info;
	
	public MyStatisticsAgregator(String filePath,int numberOfGraphSamples){
		
		this.numberOfGraphSamples=numberOfGraphSamples;
		this.filePath=filePath;
		statistics=new ArrayList<>();
		
	}
	/*
	 * Ovo nekako razdvojiti na racunanje i ispisivanje posenbo
	 */
	public void calculateStatistics() {
		totalMin=Double.MAX_VALUE;
		totalMax=0;
		totalAverage=0;//TREBA ISPISATI
		numberOfClasses=statistics.get(0).getNumberOfClasses();
	//	specificAverages= new double[numberOfClasses];//TREBA ISPISATI
		

		totalAverageSteps=new double[numberOfGraphSamples];//TREBA ISPISATI
	//	specificAccuracySteps=new double[numberOfClasses][numberOfGraphSamples];//TREBA ISPISATI
		
		//Skuplja ukupnu i specificnu krajnju tocnost iz svih statistiku i radi prosjek
		for(NetworkStatistics stats:statistics) {
			double currentStatisticTotal=stats.getTotalAccuracy();
			
			totalAverage+=currentStatisticTotal;
			totalMin=Math.min(totalMin, currentStatisticTotal);
			totalMax=Math.max(totalMax, currentStatisticTotal);
			
			double tempoSteps[]=stats.getTotalAccuracySteps();//tocnosti u trenutcima ove trenutne statistike
			
			//double[][] tempoSpecificSteps=stats.getSpecificAccuracySteps();
			
			for(int i=0;i<numberOfGraphSamples;i++) {
				totalAverageSteps[i]+=tempoSteps[i];
			}
//			for(int i=0;i<numberOfClasses;i++) {
//				specificAverages[i]+=stats.getSpecificAccuracy()[i];
//				for(int j=0;j<numberOfGraphSamples;j++) {
//					specificAccuracySteps[i][j]+=tempoSpecificSteps[i][j];
//				}
//			}
		}
		
		//Uprosjecivanje svih vrijednosti
		for(int j=0;j<numberOfGraphSamples;j++) {
			totalAverageSteps[j]/=statistics.size();
//			for(int i=0;i<numberOfClasses;i++) {
//				specificAverages[i]/=statistics.size();
//				specificAccuracySteps[i][j]/=statistics.size();
//			}
		}
		
		totalAverage/=statistics.size();
		
		
	//dumpTofile();
	}
	public void dumpTofile() {
		double count=0.1;
		DecimalFormat df = new DecimalFormat("#.#"); 
		try {
			File dir=new File(filePath+"\\"+info);
			if(!dir.exists()) {
				dir.mkdir();
			}
			PrintStream fileOut = new PrintStream(dir.getAbsolutePath()+"\\table.txt");
			System.setOut(fileOut);
			//Ispis ukupne prosjecne tocnosti
			System.out.println(info);
			System.out.println(totalMin);
			System.out.println(totalAverage);
			System.out.println(totalMax);
			
			
			
			fileOut=new PrintStream(dir.getAbsolutePath()+"\\graph.txt");
			System.setOut(fileOut);
			System.out.println("iter "+info);
			for(double d:totalAverageSteps) {
				System.out.println(df.format(count).replace(",",".")+" "+d);
			//	System.out.printf("%.1f %f\n",count,d);
				count+=0.1;
			}
			
			fileOut=new PrintStream(dir.getAbsolutePath()+"\\box.txt");
			System.setOut(fileOut);
			System.out.println(info);
			for(NetworkStatistics st:statistics) {
				System.out.println(st.getTotalAccuracy());
			}
			
			fileOut.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public NetworkStatistics getNthStatistic(int n){
		return statistics.get(n);
	}
	
	public void addStatistic(NetworkStatistics s) {
		statistics.add(s);
		s.setNumberOfGraphSamples(numberOfGraphSamples);
	}
	
	public int getNumberOfGraphSamples() {
		return numberOfGraphSamples;
	}
	public double getTotalAverage() {
		return totalAverage;
	}
	public void setInfo(String info) {
		this.info=info;
	}
}
