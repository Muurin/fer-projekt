package hr.fer.projekt.statistics;


/**
 * 
 * @author Marin Ov�ari�ek
 *
 */
public class MyNetworkStatistics implements NetworkStatistics {
	String networkInfo;
	String filePath;
	
	int numberOfClasses;
	int numberOfGraphSamples;
	int frequency;
	int stepCounter;
	
	int specificCorrectGuesses[];
	int specificGuessCounter[];
	int guessCounter;
	int correctGuesses;
	
	double[][] specificAccuracySteps;
	double[] totalAccuracySteps;
	
	double[] specificAccuracy;
	double totalAccuracy;
	
	@Override
	public void setNumberOfClasses(int numberOfClasses) {
		this.numberOfClasses=numberOfClasses;
		specificCorrectGuesses=new int[numberOfClasses];
		specificGuessCounter=new int[numberOfClasses];

	}

	@Override
	public void setNumberOfTrainSamples(int numberOfTrainSamples) {
		frequency=numberOfTrainSamples/numberOfGraphSamples;
		specificAccuracySteps=new double[numberOfClasses][numberOfGraphSamples];
		totalAccuracySteps=new double[numberOfGraphSamples];
	}

	@Override
	public void addNewSample(int ordinalOfClass, boolean correct) {
		guessCounter++;
		//specificGuessCounter[ordinalOfClass]++;
		if(correct) {
			//specificCorrectGuesses[ordinalOfClass]++;
			correctGuesses++;
		}
//		
//		if(guessCounter%frequency==0) {
//			for(int i=0;i<numberOfClasses;i++) {
//				specificAccuracySteps[i][guessCounter/frequency]=specificCorrectGuesses[i]/specificGuessCounter[i]; //vremenska tocnost pojedinacnih klasa
//		}
//			totalAccuracySteps[(guessCounter/frequency)-1]=(double)correctGuesses/guessCounter;//-1 jer indeksiranje pocinje od nule
//		}

	}

	@Override
	public void calculateAverages() {
		if(stepCounter-1==numberOfGraphSamples) {
			throw new RuntimeException("Previse puta pozvan calculate");
		}
		//System.out.println("Pozvan je step "+stepCounter+".Ima "+correctGuesses+ " tocnih od ukupno "+guessCounter);
		//izracunati kranju tocnost (ukupna i specificna)
	//	for(int i=0;i<numberOfClasses;i++) {
		//	specificAccuracy[i]=specificCorrectGuesses[i]/specificGuessCounter[i]; 
	//	}
		//System.out.println(stepCounter);
		totalAccuracySteps[stepCounter]=(double)correctGuesses/guessCounter;
		guessCounter=0;
		correctGuesses=0;
		stepCounter++;
		
		//totalAccuracy=(double)correctGuesses/guessCounter;
	}


	@Override
	public void setNetworkInfo(String info) {
		this.networkInfo=info;
	}

	public double getTotalAccuracy() {
		return totalAccuracy;
	}
	
	public double[] getSpecificAccuracy() {
		return specificAccuracy;
	}
	
	public int getNumberOfClasses() {
		return numberOfClasses;
	}
	
	public double[][] getSpecificAccuracySteps() {
		return specificAccuracySteps;
	}
	
	public double[] getTotalAccuracySteps() {
		return totalAccuracySteps;
	}
	
	public void setNumberOfGraphSamples(int n) {
		this.numberOfGraphSamples=n;
	}
	public void setTotalAccuracy(double totalAccuracy) {
		this.totalAccuracy = totalAccuracy;
	}
}
