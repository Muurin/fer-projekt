package hr.fer.projekt.artefacts;

import hr.fer.projekt.models.Matrix;
/**
 * Encapsulation of information passed around between layers in neural network.
 * @author Marin Ov�ari�ek
 *
 */
public class Artefact {
	private Matrix[] matrices;
	
	public Artefact(Matrix[] matrices) {
		this.matrices=matrices;
	}
	
	public Matrix[] getMatrices() {
		return matrices;
	}
	
}
