package hr.fer.projekt.runners;

import hr.fer.projekt.architecture.Architecture;
import hr.fer.projekt.architecture.MinimalMNISTArchitecture;
import hr.fer.projekt.dataloader.Dataset;
import hr.fer.projekt.dataloader.IDXDatasetLoader;
import hr.fer.projekt.functions.CrossEntropyOfSoftMaxDerivative;
import hr.fer.projekt.learnerFactory.ADAMFactory;
import hr.fer.projekt.learnerFactory.AdaGradFactory;
import hr.fer.projekt.learnerFactory.AdaMaxFactory;
import hr.fer.projekt.learnerFactory.GradientDescentFactory;
import hr.fer.projekt.learnerFactory.LearnerFactory;
import hr.fer.projekt.learnerFactory.MomentumFactory;
import hr.fer.projekt.learnerFactory.NadamFactory;
import hr.fer.projekt.learnerFactory.RMSPropFactory;
import hr.fer.projekt.learners.RMSProp;
import hr.fer.projekt.networks.NetworkSettings;
import hr.fer.projekt.networks.NeuralNetwork;
import hr.fer.projekt.statistics.MyStatisticsAgregator;

public class NNetworkRunner {
	//"C:\\Users\\Admin\\Desktop";
	public static String outputPath=System.getProperty("user.dir");
	public static int numberOfGraphSamples=100;
	public static int iterations=10;
	
	public static String imageFilePathTrain="train-images.idx3-ubyte";
	public static String labelFilePathTrain="train-labels.idx1-ubyte";
	
	public static String imageFilePathTest="t10k-images.idx3-ubyte";
	public static String labelFilePathTest="t10k-labels.idx1-ubyte";
	
	public static void main(String[] args) {
		MyStatisticsAgregator ag=new MyStatisticsAgregator(outputPath, numberOfGraphSamples);
		//
		//new MomentumFactory();
		//new RMSPropFactory();
		//new NadamFactory();
		//new ADAMFactory();
		//new AdaGradFactory();
		//new AdaMaxFactory();
		LearnerFactory factory=new AdaGradFactory();//new RMSPropFactory();
		Architecture arch;
		
		Dataset trainSet=new Dataset(new IDXDatasetLoader(NNetworkRunner.imageFilePathTrain,NNetworkRunner.labelFilePathTrain));
		Dataset testSet=new Dataset(new IDXDatasetLoader(NNetworkRunner.imageFilePathTest,NNetworkRunner.labelFilePathTest));
		NetworkSettings settings=new NetworkSettings(2,1,5, 10, 5);
		
		NeuralNetwork network;
		
		for(int i=0;i<iterations;i++) {
			arch= new MinimalMNISTArchitecture(factory);
			network=new NeuralNetwork(arch, trainSet, testSet, settings, ag, new CrossEntropyOfSoftMaxDerivative());
			network.train();
		}
		ag.calculateStatistics();
		ag.dumpTofile();
	}

}
