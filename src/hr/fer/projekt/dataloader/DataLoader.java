package hr.fer.projekt.dataloader;

import java.util.ArrayList;
import java.util.Set;
/**
 * Dataset loader must implements this interface to send image and desired output data to the NeuralNetwork class
 * @author Marin Ov�ari�ek
 *
 */
public interface DataLoader {
	
	public ArrayList<double[]> getImages();
	public ArrayList<Integer>   getOutput();
	public int getPictureSize();
	public Set<Integer> getClasses();
}
