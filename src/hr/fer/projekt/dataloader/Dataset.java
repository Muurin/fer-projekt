package hr.fer.projekt.dataloader;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
/**
 * Class representation of a dataset used for training the neural network.
 * @author Marin Ov�ari�ek
 *
 */
public class Dataset  {
	
	List<ImageOutputPair> data;
	int imageSize;
	Set<Integer> classes;
	
	public Dataset(DataLoader loader) {
		data=PairImagetoValue(loader.getImages(),loader.getOutput());
		imageSize=loader.getPictureSize();
		classes=loader.getClasses();
	}
	
	private List<ImageOutputPair> PairImagetoValue(ArrayList<double[]>  images , ArrayList<Integer> outputs) {
		if(images.size()!=outputs.size()) {
			System.out.println(images.size()+" "+outputs.size());
			throw new IllegalArgumentException("List of images is not the same size as list of outputs.");
		}
		
		int size=images.size();
		List<ImageOutputPair> list=new ArrayList<>();
		
		for(int i=0;i<size;i++) {
			list.add(new ImageOutputPair(images.get(i), outputs.get(i)));
		}
		return list;
	}
	
	public List<ImageOutputPair> getData() {
		return data;
	}
	public Set<Integer> getClasses() {
		return classes;
	}
}

