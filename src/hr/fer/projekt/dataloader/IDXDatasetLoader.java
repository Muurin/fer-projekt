package hr.fer.projekt.dataloader;

import java.io.IOException;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Class used for loading 2D data from idx file
 * @author Marin Ov�ari�ek
 *
 */
public class IDXDatasetLoader implements DataLoader {
	
	private String imageFilePath;
	private String labelFilePath;
	
	ArrayList<double[]> images;
	ArrayList<Integer> labels;//integer representing a label for nth class (1st class is label 0)
	private int labelHeaderOffset=8;
	private int imageFileOffset=16;
	private int pictureSize;
	private Set<Integer> classes;
	
	public IDXDatasetLoader(String imageFilePath,String labelFilePath) {
		this.imageFilePath=imageFilePath;
		this.labelFilePath=labelFilePath;
		images=new ArrayList<>();
		labels=new ArrayList<>();
		classes=new HashSet<>();
		
		readImages();
		readLabels();
	}
	
	private void readLabels() {
		byte[] labelData=null;
		try {
			labelData = Files.readAllBytes(Paths.get(labelFilePath));
			}
		catch (IOException e) {System.err.println("Error: " + e.getMessage());}
				
		if(labelData!=null) {
			int noItems;
			int position=0;
			position+=4;//skip the first magic number
			noItems=loadIntFromByteArray(labelData, position);
			position+=4;
			for(int i=0;i<noItems;i++) {
				int label=Byte.toUnsignedInt(labelData[i+labelHeaderOffset]);
				labels.add(label);
				classes.add(label);
			}
		}
	}

	private void readImages() {
		byte[] imageData=null;
		try {
			imageData = Files.readAllBytes(Paths.get(imageFilePath));
			}
		catch (IOException e) {System.err.println("Error: " + e.getMessage());}
				
		if(imageData!=null) {
			int width;
			int height;
			int noItems;
			int position=0;
			position+=4;//skip the first magic number
			noItems=loadIntFromByteArray(imageData, position);
			position+=4;
			width=loadIntFromByteArray(imageData, position);
			position+=4;
			height=loadIntFromByteArray(imageData, position);
			position+=4;
			int byteArraySize=noItems*width*height;
			pictureSize=height*width;
			int current=0;
			double[] pixelValues=new double[width*height];
			for(int i=0;i<byteArraySize;i++) {
				pixelValues[current++]=(((double)Byte.toUnsignedInt(imageData[i+imageFileOffset])/255.0));
				if((current)%pictureSize==0 &&current!=0) {
					current=0;
					images.add(pixelValues);//dodaj sliku
					pixelValues=new double[width*height];
				} 
			}
		}
		
	}
	private int loadIntFromByteArray(byte[]array ,int pos) {	
	//	int size= array[pos] | array[pos+1]  | array[pos+2]  | array[pos+3] ;//dataset size from 4bytes(int)
		byte[] bytes= {array[pos],array[pos+1],array[pos+2],array[pos+3]};
		int size=java.nio.ByteBuffer.wrap(bytes).order(ByteOrder.BIG_ENDIAN).getInt();
	//	int size= array[pos+3] | array[pos+2]  | array[pos+1]  | array[pos] ;
	//	System.out.println(size);
		return size;
	}


	@Override
	public ArrayList<double[]> getImages() {
		return images;
	}

	@Override
	public ArrayList<Integer> getOutput() {
		return labels;
	}
	
	public int getPictureSize() {
		return pictureSize;
	}
	
	public Set<Integer> getClasses() {
		return classes;
	}

}
