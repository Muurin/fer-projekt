package hr.fer.projekt.dataloader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
/**
 * Hardcoded dataset loader for MNIST handwritten digits dataset.
 * @author Marin Ov�ari�ek
 *
 */
public class DatasetLoader {
	static int NUMBER_OF_IMAGES_TRAIN =60000;
	static int NUMBER_OF_IMAGES_TEST =10000;
	static int NUMBER_OF_PIXELS =784;
	static String trainLabel = "train-labels.idx1-ubyte";
	static String trainImages = "train-images.idx3-ubyte";
	static String testImages = "t10k-images.idx3-ubyte";
	static String testLabel = "t10k-labels.idx1-ubyte";
	static int image_Fileheaderoffset =16;
	static int digit_Filehederoffset=8;
	private String labelFile;
	private String imagesFile;
	private int size;
	
	private byte[] imageData;
	private byte[] digitData;
	
	private ArrayList<Integer> digits;
	private ArrayList<Double> pixelValues;
	private ArrayList <double[]> images;
	
	public DatasetLoader(){
	}

	public ArrayList<Integer> getDigits() {
		return digits;
	}

	public ArrayList<Double> getPixels() {
		return pixelValues;
	}
	public void loadData(int mode) {
		
		digits=new ArrayList<Integer>();
		pixelValues=new ArrayList<Double>();
		images =new ArrayList<double[]>();
		//mode determines whether the data will be from training or testing files
		switch(mode) {
		case 0:
			labelFile=trainLabel;
			imagesFile=trainImages;
			size=NUMBER_OF_IMAGES_TRAIN;
			break;
		case 1:
			labelFile=testLabel;
			imagesFile=testImages;
			size=NUMBER_OF_IMAGES_TEST;
			break;
		default:
			return;
		}
		//loads digits
		try {
			 digitData = Files.readAllBytes(Paths.get(labelFile));
		 }
		  catch (IOException e) {System.err.println("Error: " + e.getMessage());}
		
		if(digitData!=null) {
			for(int i=0;i<size;i++)
				digits.add(Byte.toUnsignedInt(digitData[i+digit_Filehederoffset]));
			
			}
		//loads pixels
		try {
			imageData = Files.readAllBytes(Paths.get(imagesFile));
		 }
		  catch (IOException e) {System.err.println("Error: " + e.getMessage());}
		
		if(imageData!=null) {
			for(int i=0;i<size*784;i++)
				pixelValues.add((double) ((double)Byte.toUnsignedInt(imageData[i+image_Fileheaderoffset])/255.0));
		}
		
		createImageList(size);
	}
	public ArrayList<double[]> getImages() {
		return images;
	}
	
	public int getSize() {
		return size;
	}
	
	public void createImageList(int numberOfImages) {
		for(int i=0;i<numberOfImages;i++) {
			double[] temp=new double[NUMBER_OF_PIXELS];
			for(int j=0;j<NUMBER_OF_PIXELS;j++)
				temp[j]=pixelValues.get(i*NUMBER_OF_PIXELS+j);
			images.add(temp);
		}
	}
}
