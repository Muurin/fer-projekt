package hr.fer.projekt.dataloader;
/**
 * Class that binds image to activation of nth (outputValue) neuron in output layer.
 * @author Marin Ov�ari�ek
 *
 */
public class ImageOutputPair {
	double[] imageValues;
	int outputValue;
	
	public ImageOutputPair(double[] imageValues,int outputValue) {
		this.imageValues=imageValues;
		this.outputValue=outputValue;
	}
	
	public double[] getImageValues() {
		return imageValues;
	}
	public int getOutputValue() {
		return outputValue;
	}
}
