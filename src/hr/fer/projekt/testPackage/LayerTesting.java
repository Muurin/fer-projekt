package hr.fer.projekt.testPackage;

import hr.fer.projekt.artefacts.Artefact;
import hr.fer.projekt.functions.CrossEntropyOfSoftMaxDerivative;
import hr.fer.projekt.functions.DerivativeOfX;
import hr.fer.projekt.functions.IdentityFunction;
import hr.fer.projekt.layers.OutputLayer;
import hr.fer.projekt.learnerFactory.GradientDescentFactory;
import hr.fer.projekt.learnerFactory.LearnerFactory;
import hr.fer.projekt.models.ErrorCalculator;
import hr.fer.projekt.models.Matrix;
import hr.fer.projekt.models.MatrixUtility;

public class LayerTesting {

	public static void main(String[] args) {
//		//Testiranje poolinga-RADI
		double[] array= {1,2,3,4,5,6,7,8,9};
//		Matrix[] toBePooled= {new Matrix(3,3,array)};
//		PoolingLayer pooling=new PoolingLayer(1, 2);
//		Artefact a=new Artefact(toBePooled);
//		pooling.setForward(a);
//		a=pooling.feedForward();
//		for(Matrix m:a.getMatrices()) {
//			m.print();
//		}
		//Testiranje outputa
		
		LearnerFactory f=new GradientDescentFactory();
		OutputLayer outputLayer=new OutputLayer(4,2,f.create(),new IdentityFunction(),new DerivativeOfX());
	//	double input[]= {0.6,0.1,0.7,0.8};
	Matrix[] input={new Matrix(1, 4,new double[] {0.6,0.1,0.7,0.8})};
//		Artefact a=new Artefact(input);
//		outputLayer.setForward(a);
//		double[] output=outputLayer.feedForward().getMatrices()[0].getValues();
//		for(double d:output) {
//			System.out.println(d);
//		}
//		
//		double[] weights=outputLayer.getWeights().getValues();
//		for(double d :weights) {
//			System.out.println(d);
//		}
//		
//		ErrorCalculator erc=new ErrorCalculator(new CrossEntropyOfSoftMaxDerivative());
//		erc.set(new Artefact(new Matrix[]{new Matrix(1,4,new double[] {0.3,0.5,0.7,-0.5})}), new double[] {0,0,1,0});
//		var err=erc.computeError();
//		for(double d:err.getMatrices()[0].getValues()) {
//			System.out.println(d);
//		}
		
//		
//		MatrixUtility.transform(input[0], matrixValues->{
//			for(int i=0;i<matrixValues.length;i++) {
//				matrixValues[i]=0;
//			}
//			return matrixValues;});
//		
//		for(double d:input[0].getValues()) {
//			System.out.println(d);
//		}
		double[] der=new DerivativeOfX().apply(array);
		
		for(int i=0;i<=array.length;i++) {
			System.out.println(array[i]+" "+der[i]);
		}
	}

}
