package hr.fer.projekt.testPackage;

import java.time.Duration;
import java.time.Instant;

import hr.fer.projekt.architecture.Architecture;
import hr.fer.projekt.architecture.ExtendedMNISTArchitecture;
import hr.fer.projekt.architecture.MinimalMNISTArchitecture;
import hr.fer.projekt.dataloader.Dataset;
import hr.fer.projekt.dataloader.IDXDatasetLoader;
import hr.fer.projekt.functions.CrossEntropyOfSoftMaxDerivative;
import hr.fer.projekt.learnerFactory.ADAMFactory;
import hr.fer.projekt.learnerFactory.AdaGradFactory;
import hr.fer.projekt.learnerFactory.AdaMaxFactory;
import hr.fer.projekt.learnerFactory.GradientDescentFactory;
import hr.fer.projekt.learnerFactory.LearnerFactory;
import hr.fer.projekt.learnerFactory.MomentumFactory;
import hr.fer.projekt.learnerFactory.NadamFactory;
import hr.fer.projekt.learnerFactory.RMSPropFactory;
import hr.fer.projekt.networks.NetworkSettings;
import hr.fer.projekt.networks.NeuralNetwork;
import hr.fer.projekt.statistics.MyStatisticsAgregator;

public class NetworkTest {
	public static String outputPath="C:\\Users\\Admin\\Desktop";
	public static int numberOfGraphSamples=2;
	public static int iterations=1;
	
	public static String imageFilePathTrain="train-images.idx3-ubyte";
	public static String labelFilePathTrain="train-labels.idx1-ubyte";
	
	public static String imageFilePathTest="t10k-images.idx3-ubyte";
	public static String labelFilePathTest="t10k-labels.idx1-ubyte";
	
	public static void main(String[] args) {
		
		MyStatisticsAgregator ag=new MyStatisticsAgregator(outputPath, numberOfGraphSamples);
	
		LearnerFactory factory=new GradientDescentFactory(0.001);
		Architecture arch = new MinimalMNISTArchitecture(factory);
		Architecture arch2 =new ExtendedMNISTArchitecture(factory);
		
		Dataset trainSet=new Dataset(new IDXDatasetLoader(NetworkTest.imageFilePathTrain,NetworkTest.labelFilePathTrain));
		Dataset testSet=new Dataset(new IDXDatasetLoader(NetworkTest.imageFilePathTest,NetworkTest.labelFilePathTest));
		NetworkSettings settings=new NetworkSettings(2,100, 1, 10, 5);
		
		NeuralNetwork network;//=new NeuralNetwork(arch, trainSet, testSet, settings, ag, cesmd);//new CrossEntropyOfSoftMaxDerivative());
		Instant start=null;
		for(int i=0;i<iterations;i++) {
			network=new NeuralNetwork(arch2, trainSet, testSet, settings, ag, new CrossEntropyOfSoftMaxDerivative());
			start=Instant.now();
			network.train();
		}
		ag.calculateStatistics();
		ag.dumpTofile();
		//System.out.println(ag.getTotalAverage());
		Instant end=Instant.now();
		System.out.println(Duration.between(start, end));
	}
}
