package hr.fer.projekt.image;



public class Image {
	/**
	 * All of input images must be of resolution 28*28 with 8bit unsigned color
	 * depth,
	 */
	private static final int IMAGE_SIZE = 28;

	/** Pixels of an image. */
	byte[] pixels = new byte[IMAGE_SIZE * IMAGE_SIZE];

	/** Number represented in image. */
	int value = 0;

	/**
	 * Creates a image of size 28*28 from given input Reading start at given offset
	 * 
	 * @param value - Value of digit written in image.
	 * @throws IndexOutOfBoundsException if offset + image_size^2 > array.length
	 */
	public Image(byte[] array, int offset, int value) {
		for (int i = 0; i < IMAGE_SIZE * IMAGE_SIZE; i++) {
			pixels[i] = array[i + offset];
		}
		this.value = value;
	}

	public Image(byte[] array, int value) {
		this(array, 0, value);
	}

	public byte[] getPixels() {
		return pixels;
	}

	public int getWidth() {
		return IMAGE_SIZE;
	}

	public int getHeight() {
		return IMAGE_SIZE;
	}

	public void setPixel(int x, int y, byte value) {
		pixels[y * IMAGE_SIZE + x] = value;
	}

	public byte getPixel(int x, int y) {
		return pixels[y * IMAGE_SIZE + x];
	}
	
	public int getValue() {
		return value;
	}
}
