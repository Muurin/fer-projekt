package hr.fer.projekt.image;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ImageLoader {
	
	/**Loads images from given files. 
	 * @throws IOException if any file doesn't exist.
	 * @throws IllegalAccessException if files aren't in a valid form.
	 * */
	public static List<Image> loadImages(String pixels,String values) throws IOException{
		ArrayList<Image> list = new ArrayList<Image>();
		
		Path pixelFile = Paths.get(pixels);
		Path valuesFile = Paths.get(values);
		
		byte[] pixelsData = Files.readAllBytes(pixelFile);
		byte[] valuesData = Files.readAllBytes(valuesFile);
		
		//we check if file is valid
		if(!(pixelsData[0]==0x00 && pixelsData[1]==0x00 && pixelsData[2]==0x08 && pixelsData[3]==0x03))
			throw new IllegalArgumentException();
		
		//we check if file is valid
		if(!(valuesData[0]==0x00 && valuesData[1]==0x00 && valuesData[2]==0x08 && valuesData[3]==0x01))
			throw new IllegalArgumentException();
		
		int numberOfImages = Byte.toUnsignedInt(pixelsData[4]) << 24 | 
							 Byte.toUnsignedInt(pixelsData[5]) << 16 | 
							 Byte.toUnsignedInt(pixelsData[6]) << 8  | 
							 Byte.toUnsignedInt(pixelsData[7]);
		
		System.out.println("Number of images: " + numberOfImages);
		
		int numberOfValues = Byte.toUnsignedInt(valuesData[4]) << 24 | 
							 Byte.toUnsignedInt(valuesData[5]) << 16 | 
							 Byte.toUnsignedInt(valuesData[6]) << 8  | 
							 Byte.toUnsignedInt(valuesData[7]);

		if(numberOfImages != numberOfValues) {
			throw new IllegalArgumentException();
		}
		
		for(int i=0;i<numberOfImages;i++) {
			list.add(new Image(pixelsData, 16+28*28*i, valuesData[8+i]));
		}
		
		return list;
	}
}
