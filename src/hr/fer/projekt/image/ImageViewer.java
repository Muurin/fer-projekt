package hr.fer.projekt.image;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Objects;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * Static class used to draw an 8bit grayscale image to the screen.
 * */
public class ImageViewer extends JFrame {
	
	private static final int WINDOW_SIZE = 400;

	private static final long serialVersionUID = 1L;

	private static ImageViewer imageViewer = new ImageViewer();

	private Canvas canvas;

	static public class Canvas extends JPanel {

		private static final long serialVersionUID = 1L;

		private Image currentImage;

		public Canvas() {

		}
		/**
		 * We override paintComponent function to make it display our images instead of blank background.
		 * */
		@Override
		protected void paintComponent(Graphics g) {
			if(currentImage==null) {
				return;
			}
			g.setColor(new Color(255, 255, 255));
			Dimension dimension = this.getSize();
			g.fillRect(0, 0, dimension.width - 1, dimension.height - 1);

			int xdelta = dimension.width / currentImage.getWidth();
			int ydelta = dimension.height / currentImage.getHeight();

			for (int y = 0; y < currentImage.getHeight(); y++) {
				for (int x = 0; x < currentImage.getWidth(); x++) {
					int value = Byte.toUnsignedInt(currentImage.getPixel(x, y));
					g.setColor(new Color(255-value, 255-value, 255-value));
					g.fillRect(xdelta * x, ydelta * y, xdelta, ydelta);
				}
			}
		}
		/**
		 * Public interface used to draw given image to screen.
		 * @param image - image that will be drawn to the screen.
		 * @throws NullPointerException if image is null.
		 * */
		public void drawImage(Image image) {
			currentImage = Objects.requireNonNull(image);
			SwingUtilities.invokeLater(() -> {
				this.repaint();
			});
		}
	}

	private ImageViewer() {

		canvas = new Canvas();
		this.add(canvas);
		
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	}

	public static void drawImage(Image image) {
		imageViewer.canvas.drawImage(image);
		
		SwingUtilities.invokeLater(()->{imageViewer.setSize(WINDOW_SIZE,WINDOW_SIZE); imageViewer.setVisible(true);});
	}

}
