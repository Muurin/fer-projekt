package hr.fer.projekt.learnerFactory;

import hr.fer.projekt.learners.Learner;
import hr.fer.projekt.learners.RMSProp;
/**
 * 
 * @author Marin Ov�ari�ek
 *
 */
public class RMSPropFactory implements LearnerFactory{
	private double learningRate;
	private double momentum;
	/**
	 * Constructor with default parameters.
	 */
	public RMSPropFactory() {
		learningRate=0.001;
		momentum=0.9;
	}
	/**
	 * Custom parameter constructor.
	 * @param learningRate
	 * @param momentum
	 */
	public RMSPropFactory(double learningRate, double momentum) {
		this.learningRate=learningRate;
		this.momentum=momentum;
	}
	@Override
	public Learner create() {
		return new RMSProp(learningRate,momentum);
	}

}

