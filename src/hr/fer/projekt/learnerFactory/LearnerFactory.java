package hr.fer.projekt.learnerFactory;

import hr.fer.projekt.learners.Learner;
/**
 * 
 * @author Marin Ov�ari�ek
 *
 */
public interface LearnerFactory {
	Learner create();
}