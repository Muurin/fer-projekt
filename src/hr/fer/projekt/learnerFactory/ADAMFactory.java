package hr.fer.projekt.learnerFactory;

import hr.fer.projekt.learners.ADAM;
import hr.fer.projekt.learners.Learner;
/**
 * 
 * @author Marin Ov�ari�ek
 *
 */
public class ADAMFactory implements LearnerFactory{

	protected double learningRate;
	protected double beta1;
	protected double beta2;
	protected double epsilon=0.00000001;
	protected long step=1;
	
	
	public ADAMFactory() {
		learningRate=0.001;
		beta1=0.9;
		beta2=0.999;
	}
	public ADAMFactory(double learningRate) {
		this.learningRate=learningRate;
		beta1=0.9;
		beta2=0.999;
	}
	/**
	 * Custom parameter constructor.
	 * @param learningRate
	 * @param firstExponentialDecayRate
	 * @param secondExponentialDecayRate
	 */
	public ADAMFactory(double learningRate,double firstExponentialDecayRate,double secondExponentialDecayRate) {
		this.learningRate=learningRate;
		beta1=firstExponentialDecayRate;
		beta2=secondExponentialDecayRate;
	}
	
	@Override
	public Learner create() {
		return new ADAM(learningRate,beta1,beta1);
	}
	
}
