package hr.fer.projekt.learnerFactory;

import hr.fer.projekt.learners.GradientDescent;
import hr.fer.projekt.learners.Learner;
/**
 * 
 * @author Marin Ov�ari�ek
 *
 */
public class GradientDescentFactory implements LearnerFactory{
	
	
	private double learningRate;
	
	public GradientDescentFactory() {
		learningRate=0.001;
	}
	/**
	 * Constructor with default parameters.
	 * @param learningRate
	 */
	public GradientDescentFactory(double learningRate) {
		this.learningRate=learningRate;
	}
	
	@Override
	public Learner create() {
		return new GradientDescent(learningRate);
	}

}

