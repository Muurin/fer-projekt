package hr.fer.projekt.learnerFactory;

import hr.fer.projekt.learners.Learner;
import hr.fer.projekt.learners.Momentum;
/**
 * 
 * @author Marin Ov�ari�ek
 *
 */
public class MomentumFactory implements LearnerFactory{
	
	private double momentum;
	private double learningRate;
	
	/**
	 * Constructor with default parameters.
	 */
	public MomentumFactory() {
		learningRate=0.001;
		momentum=0.9;
	}
	
	public MomentumFactory(double learningRate) {
		this.learningRate=learningRate;
		momentum=0.9;
	}
	
	public MomentumFactory(double momentum,double learningRate) {
		this.learningRate=learningRate;
		this.momentum=momentum;
	}
	
	@Override
	public Learner create() {
		return new Momentum(momentum, learningRate);
	}

}

