package hr.fer.projekt.learnerFactory;

import hr.fer.projekt.learners.Learner;
import hr.fer.projekt.learners.Nadam;
/**
 * 
 * @author Marin Ov�ari�ek
 *
 */
public class NadamFactory extends ADAMFactory{
	
	/**
	 * Constructor with default parameters.
	 */
	public NadamFactory() {
		super();
	}
	
	public NadamFactory(double learningRate) {
		super(learningRate);
	}
	
	/**
	 * Custom parameter constructor.
	 * @param learningRate
	 * @param firstExponentialDecayRate
	 * @param secondExponentialDecayRate
	 */
	public NadamFactory(double learningRate,double firstExponentialDecayRate,double secondExponentialDecayRate) {
		super(learningRate,firstExponentialDecayRate,secondExponentialDecayRate);
	}
	
	@Override
	public Learner create() {
		return new Nadam(learningRate,beta1,beta2);
	}

}

