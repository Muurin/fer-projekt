package hr.fer.projekt.learnerFactory;

import hr.fer.projekt.learners.AdaGrad;
import hr.fer.projekt.learners.Learner;
/**
 * 
 * @author Marin Ov�ari�ek
 *
 */
public class AdaGradFactory implements LearnerFactory{
	private double learningRate;

	/**
	 * Constructor with a default learning rate.
	 */
	public AdaGradFactory() {
		learningRate=0.001;
	}
	/**
	 * Custom learning rate constructor
	 * @param learningRate
	 */
	public AdaGradFactory(double learningRate) {
		this.learningRate=learningRate;
	}
	@Override
	public Learner create() {
		return new AdaGrad(learningRate);
	}

}
