package hr.fer.projekt.learnerFactory;

import hr.fer.projekt.learners.AdaMax;
import hr.fer.projekt.learners.Learner;
/**
 * 
 * @author Marin Ov�ari�ek
 *
 */
public class AdaMaxFactory extends ADAMFactory{
	
	/**
	 * Constructor with default parameters.
	 */
	public AdaMaxFactory() {
		super();
	}
	/**
	 * Custom parameter constructor.
	 * @param learningRate
	 * @param firstExponentialDecayRate
	 * @param secondExponentialDecayRate
	 */
	public AdaMaxFactory(double learningRate,double firstExponentialDecayRate,double secondExponentialDecayRate) {
		super(learningRate,firstExponentialDecayRate,secondExponentialDecayRate);
	}
	
	@Override
	public Learner create() {
		return new AdaMax(learningRate,beta1,beta1);
	}

}

