package hr.fer.projekt.learners;
/**
 * Learner implementation of AdaGrad optimization technique.
 * @author Marin Ov�ari�ek
 *
 */
public class AdaGrad implements Learner {
	
	private double epsilon=0.00000001;
	private double learningRate;
	//private double momentum;
	private double[] lossFunctionGradientSquaredSum;
	
	/**
	 * Custom learning rate constructor
	 * @param learningRate
	 */
	public AdaGrad(double learningRate) {
		this.learningRate=learningRate;
	}
	@Override
	public double[] calculateDeltaWeights(double[] lossFunctionGradients) {
		if(lossFunctionGradientSquaredSum==null) {
			lossFunctionGradientSquaredSum=new double[lossFunctionGradients.length];
		}
		int len=lossFunctionGradients.length;
		double[] deltaW=new double[len];
		for(int i=0;i<len;i++) {
		lossFunctionGradientSquaredSum[i]+=lossFunctionGradients[i]*lossFunctionGradients[i];
		
		deltaW[i]=learningRate*lossFunctionGradients[i]
				*Math.sqrt(
						epsilon+lossFunctionGradientSquaredSum[i]);
		}
		return deltaW;
	}
	
	public Learner clone() throws CloneNotSupportedException {
		return (Learner) super.clone();
	}
	@Override
	public String toString() {
		return "AdaGrad";
	}
}
