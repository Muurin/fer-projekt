package hr.fer.projekt.learners;
/**
 * Learner implementation of Gradient descent optimization technique.
 * @author Marin Ov�ari�ek
 *
 */
public class GradientDescent implements Learner {

	private double learningRate;
	
	/**
	 * Constructor with default parameters.
	 * @param learningRate
	 */
	public GradientDescent(double learningRate) {
		this.learningRate=learningRate;
	}
	
	@Override
	public double[] calculateDeltaWeights(double[] lossFunctionGradients) {
		int len=lossFunctionGradients.length;
		double[] deltaW=new double[len];
		for(int i=0;i<len;i++) {
			deltaW[i]=lossFunctionGradients[i]*learningRate;
		}
		return deltaW;
	}
	
	public Learner clone() throws CloneNotSupportedException {
		return (Learner) super.clone();
	}
	
	@Override
	public String toString() {
		return "GradientDescent";
	}

}
