package hr.fer.projekt.learners;

import java.util.Arrays;

/**
 * Learner implementation of AdaMax optimization technique.
 * @author Marin Ov�ari�ek
 *
 */
public class AdaMax extends ADAM {
	
	private double vecNorm;
	int defaultSize=1;
	/**
	 * Custom parameter constructor.
	 * @param learningRate
	 * @param firstExponentialDecayRate
	 * @param secondExponentialDecayRate
	 */
	public AdaMax(double learningRate,double firstExponentialDecayRate,double secondExponentialDecayRate) {
		super(learningRate,firstExponentialDecayRate,secondExponentialDecayRate);
	}
	
	
	@Override
	public double[] calculateDeltaWeights(double[] lossFunctionGradients) {
		if(lossFunctionGradients.length!=lastFirstMomentEstimate.length) {
			lastFirstMomentEstimate=Arrays.copyOf(lastFirstMomentEstimate, lossFunctionGradients.length);
			lastSecondRawMomentEstimate=Arrays.copyOf(lastSecondRawMomentEstimate, lossFunctionGradients.length);
		}
		int len=lossFunctionGradients.length;
		double[] deltaW=new double[len];
		vecNorm=vectorNorm(lossFunctionGradients);
		for(int i=0;i<len;i++) {
			deltaW[i]=learningRate * BiasCorrectedFirstMomentEstimate(lossFunctionGradients[i], i)
					/infinityNormConstrainedSecondRawMomentEstimate(i);
					//BiasCorrectedSecondRawMomentEstimate(lossFunctionGradients[i], i);
					BiasedSecondRawMomentEstimate(lossFunctionGradients[i], i);
					
		}
		step++;
		return deltaW;
	}
	
	private double infinityNormConstrainedSecondRawMomentEstimate(int index) {
		
		double d= Math.max(beta2*lastSecondRawMomentEstimate[index], vecNorm);
		
		//potrebno da se spremi BiasedSecondRawMomentEstimate za sljedeci run
		
		
		return d;
	}
	
	private double vectorNorm(double[] vec) {
		double result=0;
		for(double d:vec) {
			result=Math.max(result, Math.abs(d));
		}
		return result;
	}
	
	public Learner clone() throws CloneNotSupportedException {
		return (Learner) super.clone();
	}
	@Override
	public String toString() {
		return "AdaMax";
	}
}
