package hr.fer.projekt.learners;

import java.util.Arrays;

/**
 * Learner implementation of Nadam optimization technique.
 * @author Marin Ov�ari�ek
 *
 */
public class Nadam extends ADAM {
	
	/**
	 * Custom parameter constructor.
	 * @param learningRate
	 * @param firstExponentialDecayRate
	 * @param secondExponentialDecayRate
	 */
	public Nadam(double learningRate,double firstExponentialDecayRate,double secondExponentialDecayRate) {
		super(learningRate,firstExponentialDecayRate,secondExponentialDecayRate);
	}
	
	@Override
	public double[] calculateDeltaWeights(double[] lossFunctionGradients) {
		if(lossFunctionGradients.length!=lastFirstMomentEstimate.length) {
			lastFirstMomentEstimate=Arrays.copyOf(lastFirstMomentEstimate, lossFunctionGradients.length);
			lastSecondRawMomentEstimate=Arrays.copyOf(lastSecondRawMomentEstimate, lossFunctionGradients.length);
		}
		int len=lossFunctionGradients.length;
		double[] deltaW=new double[len];
		for(int i=0;i<len;i++) {
			
			
			deltaW[i]=learningRate*
					(beta1 * BiasCorrectedFirstMomentEstimate(lossFunctionGradients[i], i) + (1-beta1)*lossFunctionGradients[i]/(1-Math.pow(beta1, step)))
					/(Math.sqrt(BiasCorrectedSecondRawMomentEstimate(lossFunctionGradients[i], i)) + epsilon);
			
		}
		step++;
		return deltaW;
	}
	
	public Learner clone() throws CloneNotSupportedException {
		return (Learner) super.clone();
	}
	
	@Override
	public String toString() {
		return "Nadam";
	}
}
