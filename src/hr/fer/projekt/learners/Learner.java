package hr.fer.projekt.learners;
/**
 * Interface used for different methods of network learning optimisation.
 * Implements deltaWeights functions which converts array of gradients to an array of changes in weights.
 * @author Marin Ov�ari�ek
 *
 */
public interface Learner extends Cloneable{
	/**
	 * Converts gradient of loss function into change in weights between neruons.
	 * @param lossFunctionGradients
	 * @return
	 */
	public double[] calculateDeltaWeights(double[] lossFunctionGradients);//vecina optimizacijskih metoda
	/**
	 * Method used for cloning a learner into a layer.
	 * @return
	 * @throws CloneNotSupportedException
	 */
	public Learner clone()throws CloneNotSupportedException;
}
