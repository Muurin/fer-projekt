package hr.fer.projekt.learners;

import java.util.Arrays;


/**
 * Learner implementation of ADAM optimization technique.
 * @author Marin Ov�ari�ek
 *
 */
public class ADAM implements Learner {
	
	protected double learningRate;
	protected double beta1;
	protected double beta2;
	protected double epsilon=0.00000001;
	protected long step=1;
	
	protected double lastFirstMomentEstimate[];
	protected double lastSecondRawMomentEstimate[];
	int defaultSize=1;
	/**
	 * Custom parameter constructor.
	 * @param learningRate
	 * @param firstExponentialDecayRate
	 * @param secondExponentialDecayRate
	 */
	public ADAM(double learningRate,double firstExponentialDecayRate,double secondExponentialDecayRate) {
		this.learningRate=learningRate;
		beta1=firstExponentialDecayRate;
		beta2=secondExponentialDecayRate;
		lastFirstMomentEstimate=new double[defaultSize];
		lastSecondRawMomentEstimate=new double[defaultSize];
	}
	
	@Override
	public double[] calculateDeltaWeights(double[] lossFunctionGradients) {
		if(lossFunctionGradients.length!=lastFirstMomentEstimate.length) {
			lastFirstMomentEstimate=Arrays.copyOf(lastFirstMomentEstimate, lossFunctionGradients.length);
			lastSecondRawMomentEstimate=Arrays.copyOf(lastSecondRawMomentEstimate, lossFunctionGradients.length);
		}
		int len=lossFunctionGradients.length;
		double[] deltaW=new double[len];
		for(int i=0;i<len;i++) {
			deltaW[i]=learningRate*BiasCorrectedFirstMomentEstimate(lossFunctionGradients[i],i)
					/(Math.sqrt(BiasCorrectedSecondRawMomentEstimate(lossFunctionGradients[i], i)) + epsilon);
			
			
		}
		step++;
		return deltaW;
	}
	
	protected double BiasedFirstMomentEstimate(double lossFunctionGradient,int index) {
		double d= (1-beta1)*lossFunctionGradient + beta1*lastFirstMomentEstimate[index];
		lastFirstMomentEstimate[index]=d;
		return d;
	}
	
	protected double BiasCorrectedFirstMomentEstimate(double lossFunctionGradient,int index) {
		return BiasedFirstMomentEstimate(lossFunctionGradient,index)
				/(1-Math.pow(beta1, step));
	}
	
	protected double BiasedSecondRawMomentEstimate(double lossFunctionGradient, int index) {
		double d=(1-beta2)*lossFunctionGradient*lossFunctionGradient + beta2*lastSecondRawMomentEstimate[index];
		lastSecondRawMomentEstimate[index]=d;
		return d;
	}
	
	protected double BiasCorrectedSecondRawMomentEstimate(double lossFunctionGradient,int index) {
		return BiasedSecondRawMomentEstimate(lossFunctionGradient, index)
				/(1-Math.pow(beta2, step));
	}
	
	public Learner clone() throws CloneNotSupportedException {
		return (Learner) super.clone();
	}
	
	@Override
	public String toString() {
		return "ADAM";
	}

}
