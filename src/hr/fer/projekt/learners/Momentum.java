package hr.fer.projekt.learners;
/**
 * Learner implementation of Momentum optimization technique.
 * @author Marin Ov�ari�ek
 *
 */
public class Momentum implements Learner {
	
	private double momentum;
	private double learningRate;
	private double lastStep[];
	
	public Momentum(double momentum,double learningRate) {
		this.learningRate=learningRate;
		this.momentum=momentum;
	}
	
	@Override
	public double[] calculateDeltaWeights(double[] lossFunctionGradients) {
		if(lastStep==null) {
			lastStep=new double[lossFunctionGradients.length];
		}
		int len=lossFunctionGradients.length;
		double[] deltaW=new double[len];
		for(int i=0;i<len;i++) {
			deltaW[i]= (learningRate*lossFunctionGradients[i]-
				momentum*lastStep[i]);
		}
		return deltaW;
	}
	
	public Learner clone() throws CloneNotSupportedException {
		return (Learner) super.clone();
	}
	@Override
	public String toString() {
		return "Momentum";
	}
}
