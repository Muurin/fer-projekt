package hr.fer.projekt.learners;

import java.util.Arrays;

/**
 * Learner implementation of RMSProp optimization technique.
 * @author Marin Ov�ari�ek
 *
 */
public class RMSProp implements Learner {

	private double learningRate;
	private double momentum;
	private double lastExcpectedValue[];
	int defaultSize=1;
	/**
	 * Custom parameter constructor.
	 * @param learningRate
	 * @param momentum
	 */
	public RMSProp(double learningRate, double momentum) {
		this.learningRate=learningRate;
		this.momentum=momentum;
		this.lastExcpectedValue=new double[defaultSize];
	}
	
	@Override
	public double[] calculateDeltaWeights(double[] lossFunctionGradients) {
		
		int len=lossFunctionGradients.length;
		double[] deltaW=new double[len];
		if(lossFunctionGradients.length!=lastExcpectedValue.length) {
			lastExcpectedValue=Arrays.copyOf(lastExcpectedValue,len);
		}
		for(int i=0;i<len;i++) {
		double expectedValue=expectedValue(lossFunctionGradients[i],i);
		deltaW[i]= learningRate*lossFunctionGradients[i]*
				expectedValue;
		}
		return deltaW;
	}
	
	private double expectedValue(double lossFunctionGradient,int index) {
		double gSquared=lossFunctionGradient*lossFunctionGradient;
		lastExcpectedValue[index]=(1-momentum)*gSquared+momentum*lastExcpectedValue[index];
		return lastExcpectedValue[index];
	}
	
	public Learner clone() throws CloneNotSupportedException {
		return (Learner) super.clone();
	}
	
	@Override
	public String toString() {
		return "RMSProp";
	}

}
